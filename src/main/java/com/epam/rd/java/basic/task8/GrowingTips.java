package com.epam.rd.java.basic.task8;

public class GrowingTips {
    private String tMeasuring;
    private int temperature;
    private String lightRequirements;
    private String wMeasuring;
    private int watering;

    public String gettMeasuring() {
        return tMeasuring;
    }

    public void settMeasuring(String tMeasuring) {
        this.tMeasuring = tMeasuring;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getLightRequirements() {
        return lightRequirements;
    }

    public void setLightRequirements(String lightRequirements) {
        this.lightRequirements = lightRequirements;
    }

    public String getwMeasuring() {
        return wMeasuring;
    }

    public void setwMeasuring(String wMeasuring) {
        this.wMeasuring = wMeasuring;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }
}
