package com.epam.rd.java.basic.task8;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private String measuring;
    private int aveLenFlower;

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public String getMeasuring() {
        return measuring;
    }

    public void setMeasuring(String measuring) {
        this.measuring = measuring;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }
}
